from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk, ImageOps
import numpy as np
from time import time
timer = 0
step = 1
p = '/home/vojtech/Downloads/Z.png'

def condition_rhomboid():
    # Skupina B
    cond_nw = np.logical_and(xix < 400, yix < 240)
    cond_ne = np.logical_and(xix >= 400, yix < 240)
    cond_se = np.logical_and(xix >= 400, yix >= 240)
    cond_sw = np.logical_and(xix < 400, yix >= 240)

    nwdiag = xix + yix > timer
    nediag = -xix+800 + yix > timer
    sediag = -xix+800 - yix+480 > timer  
    swdiag = xix -yix +480 > timer
    
    total_nw = np.logical_and(cond_nw, nwdiag)
    total_ne = np.logical_and(cond_ne, nediag)
    total_se = np.logical_and(cond_se, sediag)
    total_sw = np.logical_and(cond_sw, swdiag)
    
    #total = np.logical_or(np.logical_or(total_nw, total_ne), np.logical_or(total_se, total_sw))
    total = np.logical_or(np.logical_or(total_nw, total_ne), np.logical_or(total_se, total_sw))
    return total
    #return 0.3*(xix-400)**2 + (yix-240)**2 > timer**2

def condition_double_ellipse():
    # Skupina C
    outside_inner_ellipse = 0.2*(xix-400)**2 + (yix-240)**2 > 300**2-2*timer**2
    inside_outer_ellipse = 0.28*(xix-400)**2 + 1.4*(yix-240)**2 < timer**2
    return np.logical_or(outside_inner_ellipse, inside_outer_ellipse)

def condition_upward_chess():
    chess = np.logical_xor(yix % 24 <= 12, xix % 40 <= 20)
    first_layer = np.logical_or(chess, yix > timer)
    second_layer = yix > timer-48
    return np.logical_and(first_layer, second_layer)

def condition_bars():
    a = np.logical_xor(yix // 32 % 2 == 0, xix > timer%1024)
    b = np.logical_xor(yix // 32 % 2 == 1, 800-xix < ((10*timer)%8192)*2)
    vert = (yix // 32)/4 > timer // 512
    return vert

def offset_tiny_rhomboid(xoff=0, yoff=0, w=800, h=480):
    # Skupina B
    offset_x = xix - xoff
    offset_y = yix - yoff

    bounds_nwx = np.logical_and(offset_x > 0, offset_x < w/2)
    bounds_nwy = np.logical_and(offset_y > 0, offset_y < h/2)

    bounds_nex = np.logical_and(offset_x > 0, offset_x < w/2)
    bounds_ney = np.logical_and(offset_y > 0, offset_y < h/2)

    bounds_nwx = np.logical_and(offset_x > 0, offset_x < w/2)
    bounds_nwy = np.logical_and(offset_y > 0, offset_y < h/2)

    bounds_nwx = np.logical_and(offset_x > 0, offset_x < w/2)
    bounds_nwy = np.logical_and(offset_y > 0, offset_y < h/2)

    cond_nw = np.logical_and(offset_x< w/2, offset_y < h/2)
    cond_ne = np.logical_and(offset_x>= w/2, offset_y < h/2)
    cond_se = np.logical_and(offset_x>= w/2, offset_y >= h/2)
    cond_sw = np.logical_and(offset_x< w/2, offset_y >= h/2)

    nwdiag = offset_x+ offset_y > timer
    nediag = -offset_x+800 + offset_y > timer
    sediag = -offset_x+800 - offset_y +480 > timer  
    swdiag = offset_x- offset_y +480 > timer
    
    total_nw = np.logical_and(cond_nw, nwdiag)
    total_ne = np.logical_and(cond_ne, nediag)
    total_se = np.logical_and(cond_se, sediag)
    total_sw = np.logical_and(cond_sw, swdiag)
    
    #total = np.logical_or(np.logical_or(total_nw, total_ne), np.logical_or(total_se, total_sw))
    total = np.logical_or(np.logical_or(total_nw, total_ne), np.logical_or(total_se, total_sw))
    return total



def update_image():
    global timer, step, t0
    cond = condition_double_ellipse()
    entcond = np.expand_dims(cond, 2)
    invcond = np.bitwise_not(cond)
    entinvcond = np.expand_dims(invcond, 2)
    blend1 = srcimg * entcond
    blend2 = invimg * entinvcond
    blendsrc = Image.fromarray(np.bitwise_or(blend1, blend2))
    blend = ImageTk.PhotoImage(blendsrc)
    canvas.itemconfig(image_container, image = blend)
    canvas.imgref = blend
    timer += anim_speed*step
    if step > 0 and np.all(entinvcond) or step < 0 and not np.any(entinvcond):
        t = time()
        anim_duration = t - t0
        t0 = t
        print(f"Animation finished, duration = {round(anim_duration,3)} seconds, counter at {timer}, speed {anim_speed}")
        step = -step
    root.after(per, update_image)

if __name__ == "__main__" :
    fps = 120
    per = round(1000/fps)
    w = 800
    h = 480
    anim_speed = 5
    root = Tk()
    xix = np.indices((h,w))[1,:,:]
    yix = np.indices((h,w))[0,:,:]
    canvas= Canvas(root, width=w, height=h)
    canvas.pack()
    srcimg = Image.open(p).convert('RGB')
    srcimg = srcimg.resize((w, h), Image.ANTIALIAS)
    pixels1 = np.asarray(srcimg)
    pixels2 = 255*np.ones(np.shape(pixels1)) - pixels1
    invimg = ImageOps.invert(srcimg).transpose(Image.FLIP_LEFT_RIGHT)
    #plt.imshow(invimg)
    #plt.show()
    img = ImageTk.PhotoImage(srcimg)
    inverted = ImageTk.PhotoImage(invimg)
    image_container = canvas.create_image(0, 0, anchor = NW, image = img)

    t0 = time()
    root.after(per, update_image)
    root.mainloop()